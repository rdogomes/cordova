import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriasPostPage } from './categorias-post';

@NgModule({
  declarations: [
    CategoriasPostPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriasPostPage),
  ],
})
export class CategoriasPostPageModule {}
