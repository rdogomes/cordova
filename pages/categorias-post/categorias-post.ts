import { Component } from '@angular/core';
import { AlertController, App, IonicPage, LoadingController, NavController,NavParams} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

/************************ import PAGES ************************************/
import {CategoriasPage} from "../categorias/categorias";

/************************ import PROVIDERS ********************************/
import {DataModelProvider} from "../../providers/data-model/data-model";



/**
 * Generated class for the CategoriasPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-categorias-post',
    templateUrl: 'categorias-post.html',
})
export class CategoriasPostPage {
    newForms: FormGroup;
    public data: any;

    constructor(
        public alertCtrl: AlertController,
        public app: App,
        private formBuilder: FormBuilder,
        public loadingCtrl: LoadingController,
        public navCtrl: NavController,
        public navParams: NavParams,
        public _provider: DataModelProvider
    ) {
        this.newForms = this.formBuilder.group({
            nome: ['', Validators.compose([
                    Validators.required,
                    Validators.minLength(3)
            ])],
            imagem: [''],
            status: ['']
        });
    }

    /**
     * Função que insere novas categorias
     */
    onPost(){
        let loading = this.loadingCtrl.create({
            content: 'Processando...'
        });
        loading.present();
        if(this.newForms.valid){
            // Prepera a variável com os dados a serem inseridos
            var data = new Object({
                table: 'categorias',
                data: new Object({
                    nome: this.newForms.controls['nome'].value,
                    status: (this.newForms.controls['status'].value == false) && 0 || 1
                })
            })

            var res = this._provider.post(data);
            const response = (res as any);
            response.then( res => {
                if(res.status == true){
                    loading.dismiss();
                    var jsonAlert = new Object(
                        {title: 'Sucesso!', message: 'Cadastro efetuado com sucesso!'}
                    );
                    this.doAlert(jsonAlert);
                }
                console.log(data);
            })
        }
    }

    /**
     * Função que cria o modal ALERT
     * @param data
     */
    doAlert(data){
        let alert = this.alertCtrl.create({
            title: data.title,
            message: data.message,
            cssClass: 'my-alert',
            buttons: [
                {
                    text: 'Voltar',
                    handler: () => {
                        this.app.getActiveNav().push(CategoriasPage);
                    }
                },
            ]
        });
        alert.present();
    }

    ionViewDidLoad() {
    }

}
