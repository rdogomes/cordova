import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriasPutPage } from './categorias-put';

@NgModule({
  declarations: [
    CategoriasPutPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriasPutPage),
  ],
})
export class CategoriasPutPageModule {}
