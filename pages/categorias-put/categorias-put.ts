import { Component } from '@angular/core';
import {
    AlertController,
    App,
    IonicPage,
    LoadingController,
    NavController,
    NavParams
} from 'ionic-angular';

import {
    FormBuilder,
    FormGroup,
    Validators
} from "@angular/forms";


/************************ import PAGES ************************************/
import {CategoriasPage} from "../categorias/categorias";
import {CategoriasPostPage} from "../categorias-post/categorias-post";

/************************ import PROVIDERS********************************/
import {DataModelProvider} from "../../providers/data-model/data-model";


/**
 * Generated class for the CategoriasPutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-categorias-put',
    templateUrl: 'categorias-put.html',
})
export class CategoriasPutPage {
    public id: any;
    public categoria: any = '';
    public newForms: FormGroup;

    constructor(
        public alertCtrl: AlertController,
        public app: App,
        public formBuilder: FormBuilder,
        public loadingCtrl: LoadingController,
        public navCtrl: NavController,
        public navParams: NavParams,
        private _provider: DataModelProvider
    ) {
        this.id = this.navParams.get('id');
        this.getByCatId();

        this.newForms = this.formBuilder.group({
            id: [''],
            nome: [, Validators.compose([
                Validators.required,
                Validators.minLength(3)
            ])],
            imagem: [''],
            status: ['']
        });
    }

    /**
     * Função que retorna os dados da categoria segundo ID passado por parâmetro
     */
    getByCatId(){
        var data = new Object({
            table: 'categorias',
            id: this.id
        });

        this._provider.getById(data)
            .then(res => {
                this.categoria = res[0];
                //console.log(this.categoria)
            });
    }

    /**
     * Função que atualiza os dados
     */
    onPut(){
        if(this.newForms.valid){
            let loading = this.loadingCtrl.create({
                content: 'Processando...'
            });
            loading.present();
            // Prepera a variável com os dados a serem inseridos
            var data = new Object({
                table: 'categorias',
                data: new Object({
                    nome: this.newForms.controls['nome'].value,
                    status: (this.newForms.controls['status'].value == false && 0 | 1)
                }),
                id: this.id
            });
            var res = this._provider.put(data);
            loading.dismiss();
            const response = (res as any);
            response.then(data => {
                var jsonResponse = new Object({
                    title: 'Atualizado!',
                    message: 'Registro atualizado com sucesso!'
                });

                this.doAlert(jsonResponse);
            });
        }

    }

    /**
     * Função que cria o modal ALERT
     * @param data
     */
    doAlert(data){
        let alert = this.alertCtrl.create({
            title: data.title,
            message: data.message,
            cssClass: 'my-alert',
            buttons: [
                {
                    text: 'Categorias',
                    handler: () => {
                        this.app.getActiveNav().push(CategoriasPage);
                    }
                },
                {
                    text: 'Novo',
                    cssClass: 'secondary',
                    handler: () => {
                        this.app.getActiveNav().push(CategoriasPostPage);
                    }
                },
            ]
        });
        alert.present();
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad CategoriasPutPage');
    }

}
