import { ActionSheetController, AlertController, App, LoadingController, IonicPage, NavController, NavParams } from "ionic-angular";
import { Component } from '@angular/core';

/************************ import PAGES ************************************/
import { CategoriasPostPage } from "../categorias-post/categorias-post";
import { CategoriasPutPage } from "../categorias-put/categorias-put";

/************************ import PROVIDERS********************************/
import {DataModelProvider} from "../../providers/data-model/data-model";

/**
 * Generated class for the CategoriasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-categorias',
    templateUrl: 'categorias.html',
})
export class CategoriasPage {
    public collection: any;
    descending: boolean = false;
    order: number;
    column: string ='nome';

    public data: any;

    public id: any;
    public categoria: any = '';

    constructor(
        public actSheetsCtrl: ActionSheetController,
        public alertCtrl: AlertController,
        public app: App,
        public loadingCtrl: LoadingController,
        public navCtrl: NavController,
        public navParams: NavParams,
        private _provider: DataModelProvider
    ) {
        this.getAll();
    }

    /**
     * Retorna todas as categorias
     */
    getAll(){
        var obj = new Object({
            table: 'categorias'
        });

        this._provider.getAll(obj)
            .then(data => {
                this.collection = data;
            })
    }

    /**
     * Cria o menu de ações
     * @param id
     */
    presentActionSheet(id){
        let actSheet = this.actSheetsCtrl.create({
            title: 'O que deseja?',
            buttons: [
                {
                    text: 'Editar',
                    handler: () => {
                        this.getPutPage(id);
                    }
                },
                {
                    text: 'Excluir',
                    role: 'destructive',
                    handler: () => {
                        this.onDelete(id);
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel'
                }
            ]
        });
        actSheet.present();
    }


    /**
     * Funções que navega entre as paginas
     */
    push(page){
        this.app.getActiveNav().push(page);
        //this.navCtrl.setRoot(page);
    }

    getPostPage(){
        this.app.getActiveNav().push(CategoriasPostPage);
    }

    getPutPage(id){
        this.app.getActiveNav().push(CategoriasPutPage, {'id': id});
    }

    /**
     * Função que ordena as categorias
     * @param ev
     */
    sort(){
        this.descending = !this.descending;
        this.order = this.descending ? 1 : -1;
    }

    /**
     * Função que exclui a categoria segundo ID passado por parâmetro
     */
    onDelete(id){
        var obj = new Object({
            table: 'categorias',
            id: id
        });

        this._provider.delete(obj)
            .then(data => {
                this.categoria = data[0];
                console.log(this.categoria)
                var jsonAlert = new Object(
                    {title: 'Sucesso!', message: 'Cadastro excluído com sucesso!'}
                );
                this.doAlert(jsonAlert);
            });
    }


    /**
     * Função que cria o modal ALERT
     * @param data
     */
    doAlert(data){
        let alert = this.alertCtrl.create({
            title: data.title,
            message: data.message,
            cssClass: 'my-alert',
            buttons: [
                {
                    text: 'Fechar',
                    handler: () => {
                        this.getAll();
                    }
                },
            ]
        });
        alert.present();
    }


    ionViewDidLoad() {

    }

}
