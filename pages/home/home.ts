import { Component } from '@angular/core';
import {App, NavController} from 'ionic-angular';

import { CategoriasPage} from "../categorias/categorias";
import { ProdutosPage} from "../produtos/produtos";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    constructor(public navCtrl: NavController, protected app: App) {
    }

}
