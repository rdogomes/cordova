import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProdutosPostPage } from './produtos-post';

@NgModule({
  declarations: [
    ProdutosPostPage,
  ],
  imports: [
    IonicPageModule.forChild(ProdutosPostPage),
  ],
})
export class ProdutosPostPageModule {}
