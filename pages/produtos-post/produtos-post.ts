import { Component } from '@angular/core';
import {
    AlertController, IonicPage, LoadingController, NavController, NavParams,
    PopoverController
} from 'ionic-angular';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DataModelProvider} from "../../providers/data-model/data-model";

/**
 * Generated class for the ProdutosPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-produtos-post',
    templateUrl: 'produtos-post.html',
})
export class ProdutosPostPage {

    // Used for Read Barcode and Qr-Code
    public products: any;
    public selectedProduct: any;
    private productFound: boolean = false;

    newForms: FormGroup;

    constructor(
        private alertCtrl: AlertController,
        private _barcodeScanner: BarcodeScanner,
        public _formBuilder: FormBuilder,
        private loadingCtrl: LoadingController,
        public navCtrl: NavController,
        public navParams: NavParams,
        public _popverCtrl: PopoverController,
        private _provider: DataModelProvider
    ) {
        this.newForms = this._formBuilder.group({
            codigo: ['', Validators.compose([
                Validators.required,
                Validators.minLength(4)
            ])],
            nome: ['', Validators.compose( [
                Validators.required,
                Validators.minLength(3)
            ])],
            preco: ['', Validators.compose( [
                Validators.required,
                Validators.minLength(4),
                Validators.pattern(/^\d+.?\d*$/)
            ])],
            status:[''],
            categoria: ['',Validators.compose([
                Validators.required,
                Validators.minLength(1)
            ])]
        });
    }


    /**
     * Função que insere novas categorias
     */
    onPost(){
        let loading = this.loadingCtrl.create({
            content: 'Processando...'
        });
        loading.present();
        if(this.newForms.valid){
            // Prepera a variável com os dados a serem inseridos
            var data = new Object({
                table: 'categorias',
                data: new Object({
                    nome: this.newForms.controls['nome'].value,
                    status: (this.newForms.controls['status'].value == false) && 0 || 1
                })
            })

            var res = this._provider.post(data);
            const response = (res as any);
            response.then( res => {
                if(res.status == true){
                    loading.dismiss();
                    var jsonAlert = new Object(
                        {title: 'Sucesso!', message: 'Cadastro efetuado com sucesso!'}
                    );
                    this.doAlert(jsonAlert);
                }
                console.log(data);
            })
        }
    }

    /**
     * Read Barcode and QR-Code
     */
    scan(){
        this.selectedProduct = {};
        this._barcodeScanner.scan().then((barcodeData) => {
            this.selectedProduct = this.products.find(product => product.plu === barcodeData.text);
            if(this.selectedProduct !== undefined){
                var obj = new Object({
                    title: 'Scan Codes',
                    message: barcodeData.text
                })
                this.doAlert(obj);

                /*this._toast.show(barcodeData.text, '5000', 'center').subscribe(
                    toast => {

                    }
                );*/
                this.productFound = true;
            }else{
                this.productFound = false;
                /*this._toast.show('Produto não encontrado', '5000', 'center').subscribe(
                    toast => {
                        console.log()
                    }
                )*/
            }
        },(err) => {
            var obj = new Object({
                title: 'Scan Codes',
                message: err
            })
            this.doAlert(obj);
            /*this._toast.show(err, '5000', 'center').subscribe(
            toast => {
                console.log(toast)
            });*/
        });
    }

    /**
     * Função que cria o modal ALERT
     * @param data
     */
    doAlert(data){
        let alert = this.alertCtrl.create({
            title: data.title,
            message: data.message,
            cssClass: 'my-alert',
            buttons: [
                {
                    text: 'Fechar',
                    handler: () => {
                        //this.getAll();
                    }
                },
            ]
        });
        alert.present();
    }

    ionViewDidLoad() {
    }

}
