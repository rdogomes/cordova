import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProdutosPutPage } from './produtos-put';

@NgModule({
  declarations: [
    ProdutosPutPage,
  ],
  imports: [
    IonicPageModule.forChild(ProdutosPutPage),
  ],
})
export class ProdutosPutPageModule {}
