import { Component } from '@angular/core';
import {
    ActionSheetController,
    AlertController,
    App,
    IonicPage,
    NavController,
    NavParams,
    Platform
} from 'ionic-angular';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {Toast} from "@ionic-native/toast";

/************************ import PAGES ************************************/
import {ProdutosPostPage} from "../produtos-post/produtos-post";
import{ProdutosPutPage} from "../produtos-put/produtos-put";
import {CategoriasPutPage} from "../categorias-put/categorias-put";
import {CategoriasPostPage} from "../categorias-post/categorias-post";

/************************ import PROVIDERS ********************************/
import {DataModelProvider} from "../../providers/data-model/data-model";



/**
 * Generated class for the ProdutosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-produtos',
    templateUrl: 'produtos.html',
})
export class ProdutosPage {
    // Property used to store the callback of the event handler to unsubscribe to it when leaving this page
    public unregisterBackButtonAction: any;

    public collection: any;
    descending: boolean = false;
    order: number;
    column: string ='nome';

    public produto: any = '';

    // Used for Read Barcode and Qr-Code
    public products: any;
    public selectedProduct: any;
    private productFound: boolean = false;

    constructor(
        public actSheetsCtrl: ActionSheetController,
        public alertCtrl: AlertController,
        public app: App,
        private _barcodeScanner: BarcodeScanner,
        public navCtrl: NavController,
        public navParams: NavParams,
        public platform: Platform,
        private _toast: Toast,
        private _provider: DataModelProvider
    ) {
        this.getAll();
    }

    ionViewDidLoad() {
        this.initializeBackButtonCustomHandler();
    }

    ionViewWillLeave() {
        // Unregister the custom back button action for this page
        this.unregisterBackButtonAction && this.unregisterBackButtonAction();
    }

    initializeBackButtonCustomHandler(): void {
        this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
            console.log('Prevent Back Button Page Change');
        }, 101); // Priority 101 will override back button handling (we set in app.component.ts) as it is bigger then priority 100 configured in app.component.ts file */
    }

    /**
     * Função que retorna os produtos
     */
    getAll(){
        // Prepara as informações para o Provider
        var data = new Object({
            table: 'produtos'
        });

        this._provider.getAll(data)
            .then(data => {
                this.collection = data;
            });
    }

    /**
     * Função que exclui o produto segundo ID passado por parâmetro
     */
    onDelete(id){
        // Prepara as informações para o Provider
        var data = new Object({
            table: 'produtos',
            id: id
        });

        this._provider.delete(data)
            .then(res => {
                this.produto = res[0];

                var jsonAlert = new Object(
                    {title: 'Sucesso!', message: 'Cadastro excluído com sucesso!'}
                );
                this.doAlert(jsonAlert);
            });
    }

    /**
     * Função que cria o modal ALERT
     * @param data
     */
    doAlert(data){
        let alert = this.alertCtrl.create({
            title: data.title,
            message: data.message,
            cssClass: 'my-alert',
            buttons: [
                {
                    text: 'Fechar',
                    handler: () => {
                        this.getAll();
                    }
                },
            ]
        });
        alert.present();
    }

    /**
     * Cria o menu de ações
     * @param id
     */
    presentActionSheet(id){
        let actSheet = this.actSheetsCtrl.create({
            title: 'O que deseja?',
            buttons: [
                {
                    text: 'Editar',
                    handler: () => {
                        this.getPutPage(id);
                    }
                },
                {
                    text: 'Excluir',
                    role: 'destructive',
                    handler: () => {
                        this.onDelete(id);
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel'
                }
            ]
        });
        actSheet.present();
    }

    getPostPage(){
        this.app.getActiveNav().push(ProdutosPostPage);
    }

    getPutPage(id){
        this.app.getActiveNav().push(ProdutosPutPage, {'id': id});
    }

    /**
     * Função que ordena os Produtos
     * @param ev
     */
    sort(){
        this.descending = !this.descending;
        this.order = this.descending ? 1 : -1;
    }


    scan(){
        this.selectedProduct = {};
        this._barcodeScanner.scan().then((barcodeData) => {
            this.selectedProduct = this.products.find(product => product.plu === barcodeData.text);
            if(this.selectedProduct !== undefined){
                var obj = new Object({
                    title: 'Scan Codes',
                    message: barcodeData.text
                })
                this.doAlert(obj);

                /*this._toast.show(barcodeData.text, '5000', 'center').subscribe(
                    toast => {

                    }
                );*/
                this.productFound = true;
            }else{
                this.productFound = false;
                /*this._toast.show('Produto não encontrado', '5000', 'center').subscribe(
                    toast => {
                        console.log()
                    }
                )*/
            }
        },(err) => {
            var obj = new Object({
                title: 'Scan Codes',
                message: err
            })
            this.doAlert(obj);
            /*this._toast.show(err, '5000', 'center').subscribe(
            toast => {
                console.log(toast)
            });*/
        });
    }


}
