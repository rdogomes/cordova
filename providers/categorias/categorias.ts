import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Platform} from "ionic-angular";


/*
  Generated class for the CategoriasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategoriasProvider {
    public dados: any;
    public basepath = '/app-api';

    constructor(
        public http: HttpClient,
        private _platform: Platform

    ) {
        if(this._platform.is('cordova')){
            this.basepath = 'http://localhost/phone_gap/myapps/app-api';
        }
    }

    getAllCategories(){

        /*if(this.dados){
            return Promise.resolve(this.dados);
        }*/

        return new Promise(resolve => {
            this.http.get(this.basepath + '/api/categorias').subscribe(data => {
                this.dados = data;
                resolve(this.dados);
            });
        });
    }

    /**
     * Função que retorna a categoria conforme Id passado por parametro
     * @param id
     * @returns {Promise<any>}
     */
    getCategoryById(id){

        /*if(this.dados){
            return Promise.resolve(this.dados);
        }*/

        return new Promise(resolve => {
            this.http.get(this.basepath + '/api/categorias/'+id, {
                //params: new HttpParams().set('id', id)
            })
            .subscribe(data => {
                this.dados = data;
                resolve(this.dados);
            })
        });
    }

    /**
     * Função que insere novo registro
     * @param data
     * @returns {Promise<any>}
     */
    addCategory(data){
        return new Promise((resolve, reject)  => {
            this.http.post(this.basepath + '/api/categorias', data)
                .subscribe(res => {
                    //console.log((res));
                    resolve(res);
                },
                (err) => {
                    //console.log(err);
                    reject(err);
                }
            )

        })
    }

    /**
     * Função que atualiza os dados de um registro conforme ID passado por parametro
     * @param data
     * @param id
     * @returns {Promise<any>}
     */
    putCategory(data, id){
        return new Promise((resolve, reject) => {
            this.http.put(this.basepath + '/api/categorias/'+ id, data)
                .subscribe(res =>{
                    //console.log(res)
                    resolve(res);
                }),
                (err) => {
                    //console.log(err)
                    reject(err);
                }
        })
    }

    /**
     * Função que exclui um registro conforme ID passado por parametro
     * @param id
     * @returns {Promise<any>}
     */
    delete(id){
        return new Promise((resolve, reject) => {
            this.http.delete(this.basepath + '/api/categorias/'+ id)
                .subscribe(res =>{
                    console.log(res)
                    resolve(res);
                }),
                (err) => {
                    console.log(err)
                    reject(err);
                }
        })
    }

}
