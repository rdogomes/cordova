import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Platform} from "ionic-angular";

/*
  Generated class for the DataModelProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class DataModelProvider {
    public dados: any;
    public basepath = '/app-api';

    constructor(
        public http: HttpClient,
        private _platform: Platform
    ) {
        if(this._platform.is('cordova')){
            this.basepath = 'http://localhost/phone_gap/myapps/app-api';
        }
    }

    /**
     * Função que retorna todos os registros
     * @param data Object que contem as infos necessárias para o consumo da API
     * @returns {Promise<any>}
     */
    getAll(data){
        return new Promise(resolve => {
            this.http.get(this.basepath + '/api/'+data.table).subscribe(data => {
                this.dados = data;
                resolve(this.dados);
            });
        });
    }

    /**
     * Função que retorna a categoria conforme Id passado por parametro
     * @param data Object que contem as infos necessárias para o consumo da API
     * @returns {Promise<any>}
     */
    getById(data){
        return new Promise(resolve => {
            this.http.get(this.basepath + '/api/'+data.table+'/'+data.id, {
                //params: new HttpParams().set('id', id)
            })
                .subscribe(data => {
                    this.dados = data;
                    resolve(this.dados);
                })
        });
    }

    /**
     * Função que insere novo registro
     * @param data Object que contem as infos necessárias para o consumo da API
     * @returns {Promise<any>}
     */
    post(data){
        return new Promise((resolve, reject)  => {
            this.http.post(this.basepath + '/api/'+data.table, data.data)
                .subscribe(res => {
                        //console.log((res));
                        resolve(res);
                    },
                    (err) => {
                        //console.log(err);
                        reject(err);
                    }
                )

        })
    }

    /**
     * Função que atualiza os dados de um registro conforme ID passado por parametro
     * @param data Object que contem as infos necessárias para o consumo da API
     * @returns {Promise<any>}
     */
    put(data){
        return new Promise((resolve, reject) => {
            this.http.put(this.basepath + '/api/'+data.table+'/'+ data.id, data.data)
                .subscribe(res =>{
                    //console.log(res)
                    resolve(res);
                }),
                (err) => {
                    //console.log(err)
                    reject(err);
                }
        })
    }

    /**
     * Função que exclui um registro conforme ID passado por parametro
     * @param data Object que contem as infos necessárias para o consumo da API
     * @returns {Promise<any>}
     */
    delete(data){
        return new Promise((resolve, reject) => {
            this.http.delete(this.basepath + '/api/'+data.table+'/'+ data.id)
                .subscribe(res =>{
                    //console.log(res)
                    resolve(res);
                }),
                (err) => {
                    //console.log(err)
                    reject(err);
                }
        })
    }

}
