import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Platform} from "ionic-angular";

/*
  Generated class for the ProdutosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProdutosProvider {
    public dados: any;
    private basepath: string = '/app-api';

    constructor(
        public http: HttpClient,
        private _platform: Platform
    ) {
        if(this._platform.is('cordova')){
            this.basepath = 'http://localhost/phone_gap/myapps/app-api';
        }
    }

    /**
     * Função que retorna todos os produtos
     * @returns {Promise<any>}
     */
    getAll(){
        return new Promise(resolve => {
            this.http.get(this.basepath + '/api/produtos').subscribe(data => {
                this.dados = data;
                resolve(this.dados);
            });
        });
    }

    /**
     * Função que retorna produto conforme ID passado por parametro
     * @param id
     * @returns {Promise<any>}
     */
    getById(id){
        return new Promise(resolve => {
            this.http.get(this.basepath + '/api/produtos'+id).subscribe( data => {
                this.dados = data;
                resolve(this.dados);
            });
        });
    }

    /**
     * Função que insere novos produtos
     * @param data
     * @returns {Promise<any>}
     */
    addProduct(data){
        return new Promise((resolve, reject) => {
            this.http.post(this.basepath + '/api/produtos', data)
                .subscribe(res => {
                    resolve(res);
                }),
                (err) => {
                    reject(err);
                }
        });
    }

    /**
     * Função que atualiza o produto conforme ID passado por parametro
     * @param data
     * @param id
     * @returns {Promise<any>}
     */
    putProduct(data, id){
        return new Promise((resolve, reject) => {
            this.http.put(this.basepath + '/api/produtos/'+id, data)
                .subscribe(res => {
                    resolve(res)
                }),
                (err) => {
                    reject(err);
                }
        });
    }

    /**
     * Função que exclui registro conforme ID passado por parametro
     * @param data
     * @returns {Promise<any>}
     */
    delete(data){
        return new Promise((resolve, reject) => {
            this.http.delete(this.basepath + '/api/'+data.table+'/'+data.id)
                .subscribe( res => {
                    resolve(res);
                }),
                (err) => {
                    reject(err);
                }
        });
    }
}
